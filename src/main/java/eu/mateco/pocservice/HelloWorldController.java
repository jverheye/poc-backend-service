package eu.mateco.pocservice;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController {

  @GetMapping("/hello-world")
  public @ResponseBody String helloWorld() {
    return "HelloWorld";
  }

  @GetMapping("/hello-user")
  public ResponseEntity<String> helloUser(final HttpServletRequest request) {
    if (request.getHeader("X-User-Id") == null ||request.getHeader("X-User-Id").trim().equals("")) {
      return ResponseEntity.status(401).build();
    }
    return ResponseEntity.status(200).body("Hello " + request.getHeader("X-User-Id"));
  }
}
