package eu.mateco.pocservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocserviceApplication.class, args);
	}

}
